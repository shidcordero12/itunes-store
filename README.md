# Text Application for Appetiser

This is an example Android Application created for Appetiser online coding exam.

## Installation
Clone this repository and import into **Android Studio**

## Technology, Plugins and Library Used
1. For Support library - [AndroidX](https://developer.android.com/jetpack/androidx/)
2. IoC library - [GreenRobot EventBus ported into Android Annotation](https://github.com/tmtron/green-annotations)
3. Image Caching - [Glide](https://github.com/bumptech/glide)
4. Permission Checking - [Android Permissions](https://github.com/nabinbhandari/Android-Permissions)
5. Progress Dialog Design - [SpinKit ported to Android](https://github.com/ybq/Android-SpinKit)
6. Android Object Query - [C# linq ported to Android](https://github.com/zbra-solutions/android-linq)
7. Object Mapper - [Jackson Core](https://github.com/FasterXML/jackson-core) 

## Application Architecture

1. Follows the Android Annotation Architecture specifically for its use

   -- Main Site - http://androidannotations.org/

   -- Github - https://github.com/androidannotations/androidannotations/wiki

Why did I choose this pattern?

   -- for small and simpler projects, using Android Annotation can do the job done and in a timely manner. Less decoupled code and works perfectly. Never encountered an issue in terms of performance.

2. Code Structure
   -- This is the structure of the code 

   -- the code structure is specifically designed by me base on what I experienced mostly with the other design structure like MVP and MVVM pattern

- base - stores the application base classes
- domain - stores the classes for data related (access to remote or local storage)
- exception - stores the classes for custom exception
- messages - stores the application messages use for pub/sub event
- model - stores the response and request models
- ui - stores the ui related part. Each ui has its own package (i.e track consist of 2 package for its list and detailed view)
- util - stores any utility classes

## Data Persistence
1. I use Android Annotation Shared Preference implementation as discussed [here](https://github.com/androidannotations/androidannotations/wiki/SharedPreferencesHelpers)
2. For more secure data persistent specifically for confidential data, I use this [library](https://github.com/adorsys/secure-storage-android) 
3. Implementation for data persistence in the app is one of the suggested "the last screen that the user was on. When the app restores, it should present the user with that screen."





