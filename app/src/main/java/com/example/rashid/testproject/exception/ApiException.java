package com.example.rashid.testproject.exception;

import org.springframework.web.client.RestClientException;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Custom exception for Api errors
 * Extends RestClientException
 */
public class ApiException extends RestClientException {

    public ApiException(String msg) {
        super(msg);
    }

    public ApiException(String msg, Throwable ex) {
        super(msg, ex);
    }
}
