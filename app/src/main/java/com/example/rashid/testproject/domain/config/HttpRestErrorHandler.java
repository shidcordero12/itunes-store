package com.example.rashid.testproject.domain.config;

import android.accounts.NetworkErrorException;

import com.example.rashid.testproject.R;
import com.example.rashid.testproject.exception.ApiException;
import com.example.rashid.testproject.utils.AndroidUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.rest.spring.api.RestErrorHandler;
import org.springframework.core.NestedRuntimeException;
import org.springframework.web.client.RestClientException;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Http Rest API main error handler class to handle all
 * errors encounter and to customize result to be shown
 */
@EBean
public class HttpRestErrorHandler implements RestErrorHandler {

    /**
     * Injects the utility classes
     */
    @Bean
    AndroidUtils androidUtils;

    /**
     * Overrides the onRestClientExceptionThrown of RestErrorHandler to
     * customize the error handling.
     * User can customize the message to be shown to the user
     */
    @Override
    public void onRestClientExceptionThrown(NestedRuntimeException ex) {
        // checks if exception thrown is a Custom Exception class
        if (ex.getCause() instanceof NetworkErrorException || ex.getCause() instanceof ApiException){
            // throw message of the Custom Exception class, to be handled by catch in Service classes
            throw new RestClientException(ex.getCause().getMessage());
        }
        // throw common error message, to be handled by catch in Service classes
        throw new RestClientException(androidUtils.getMessage(R.string.em_error_occurred));
    }
}
