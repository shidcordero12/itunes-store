package com.example.rashid.testproject.exception;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Custom exception for SharedPreference errors
 * Extends RestClientException
 */
public class SharedPreferenceException extends Exception {
    // Sets the default message
    private String message = "Invalid shared preference value";

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
