package com.example.rashid.testproject.messages;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Base Message class for Event Bus
 */
public class BaseMessage {

    /**
     * Holds the instance of the sender class
     */
    private final Object sender;

    /**
     * Constructor method for BaseMessage
     * @param sender instance of sender
     */
    public BaseMessage(Object sender){
        this.sender = sender;
    }

    /**
     * Method for getting the sender of the message
     * @return Object instance of the sender
     */
    public Object getSender() {
        return sender;
    }

}
