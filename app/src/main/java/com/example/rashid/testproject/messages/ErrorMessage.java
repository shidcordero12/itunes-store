package com.example.rashid.testproject.messages;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Message class for Common Errors
 */
public class ErrorMessage extends BaseMessage{

    /**
     * Holds the error message
     */
    private String message;

    /**
     * Method for getting the error message
     * @return String error message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Constructor for ErrorMessage class
     * @param sender the sender of the message
     * @param message the error message
     */
    public ErrorMessage(Object sender, String message) {
        super(sender);

        this.message = message;
    }
}
