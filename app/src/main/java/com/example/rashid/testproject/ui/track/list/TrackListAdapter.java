package com.example.rashid.testproject.ui.track.list;

import android.content.Context;
import android.view.ViewGroup;

import com.example.rashid.testproject.R;
import com.example.rashid.testproject.base.BaseRecyclerViewAdapter;
import com.example.rashid.testproject.base.BaseViewWrapper;
import com.example.rashid.testproject.model.response.TrackResponseModel;
import com.example.rashid.testproject.ui.track.detail.ItemDetailFragment;
import com.example.rashid.testproject.ui.track.detail.ItemDetailFragment_;
import com.example.rashid.testproject.ui.track.detail.TrackDetailActivity_;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * A Track List adapter for recycler view
 */
public class TrackListAdapter extends BaseRecyclerViewAdapter<TrackResponseModel, TrackItemView> {

    /**
     * Holds the instance if the page is shown in two pane
     */
    private boolean isTwoPane;
    /**
     * Holds the instance of Root Context
     */
    private Context context;

    /**
     * Constructor method for TrackListAdapter
     * @param context instance of application context
     * @param isTwoPane instance of two pane value
     */
    TrackListAdapter(Context context, boolean isTwoPane) {
        // set instance from calling activity
        this.context = context;
        this.isTwoPane = isTwoPane;
        // initialize item list
        items = new ArrayList<>();
    }

    /**
     * Method for updating track list
     * @param trackList List<TrackResponseModel> holds the list of track value
     */
    void updateTrack(List<TrackResponseModel> trackList){
        // check if size is not 0
        if (trackList.size() != 0) {
            // add items to the Recycler view list
            items.addAll(0, trackList);
            notifyItemRangeInserted(0, trackList.size());
        }
    }

    /**
     * Overrides the method for binding the view holder
     * @param holder instance of item view holder
     * @param position the position index
     */
    @Override
    public void onBindViewHolder(@NonNull BaseViewWrapper<TrackItemView> holder, int position) {
        TrackItemView view = holder.getView();
        // gets the TrackResponseModel by index
        TrackResponseModel trackResponseModel = items.get(position);

        // binds the TrackResponseModel to the view
        view.bind(trackResponseModel);
        // sets the onclick listener of the view
        view.setOnClickListener(v -> redirectToDetailView(trackResponseModel));
    }

    /**
     * Overrides the getter for item view type
     * @param position the position index
     * @return int view id
     */
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**
     * Overrides the create item view for TrackItemView
     * @param parent instance of the view group parent
     * @param viewType instrance of the view type id
     * @return TrackItemView instance of TrackItemView
     */
    @Override
    protected TrackItemView onCreateItemView(ViewGroup parent, int viewType) {
        // builds the TrackItemView by calling the generated class
        return TrackItemView_.build(context);
    }

    /**
     * Method for redirection
     * Checks isTwoPane value
     * true - shows the description on the second pane
     * false - redirect to TrackDetailActivity
     * @param trackResponseModel instance of TrackResponseModel
     */
    void redirectToDetailView(TrackResponseModel trackResponseModel){
        // check if view is two pane
        if (isTwoPane) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            ItemDetailFragment fragment = ItemDetailFragment_.builder()
                    .setOneFragmentArg(trackResponseModel)
                    .build();
            TrackListActivity activity = (TrackListActivity) context;
            activity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.flTrackDetailContainer, fragment)
                    .commit();
        } else {
            // intent to TrackDetailActivity
            TrackDetailActivity_
                    .intent(context).trackResponseModel(trackResponseModel)
                    .start();
        }
    }
}
