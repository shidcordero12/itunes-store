package com.example.rashid.testproject.domain;

import com.example.rashid.testproject.domain.local.LastVisitedSharedPref_;
import com.example.rashid.testproject.exception.SharedPreferenceException;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Shared Preference Service for all persistent data call
 */
@EBean(scope = EBean.Scope.Singleton)
public class SharedPreferenceService {

    /**
     * Injects LastVisitedSharedPreference instance
     * Note to use the generated classes to use
     * the following method.
     * You can refer here: https://github.com/androidannotations/androidannotations/wiki/SharedPreferencesHelpers
     */
    @Pref
    LastVisitedSharedPref_ sharedPref;

    /**
     * Method to check if there's last visited page visited by the user
     * @return true|false    the value if there's a saved last visited page
     */
    public boolean hasLastVisitedPage(){
        // get the track id
        String trackId = sharedPref.trackId().get();
        // default value for trackId is "" (empty string)
        return !trackId.equals("");
    }

    /**
     * Method for getting the last visited page track id
     * @return trackId - float the value of track id
     * @throws SharedPreferenceException Custom exception for invalid operation
     */
    public float getLastVisitedPage() throws SharedPreferenceException {
        // get the track id
        String trackId = sharedPref.trackId().get();
        // check if it is the default value
        if (trackId.equals("")){
            // throw an exception. This should not be happen.
            throw new SharedPreferenceException();
        }
        // parse the track id value to float and return
        return Float.valueOf(trackId);
    }

    /**
     * Method for setting the last visited page track id
     * @param newTrackId float value of track id
     */
    public void setLastVisitedPage(float newTrackId){
        sharedPref.trackId()
                .put(String.valueOf(newTrackId));
    }

    /**
     * Method for clearing all the values inside the
     * LastVisitedSharedPreference
     */
    public void clearLastVisitedPage(){
        sharedPref.clear();
    }
}
