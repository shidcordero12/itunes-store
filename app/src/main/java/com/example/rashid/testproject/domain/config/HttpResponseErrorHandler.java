package com.example.rashid.testproject.domain.config;

import android.content.Context;

import com.example.rashid.testproject.exception.ApiException;
import com.example.rashid.testproject.model.response.ErrorResponseModel;
import com.example.rashid.testproject.utils.AndroidUtils;
import com.example.rashid.testproject.utils.RestApiUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;

import java.io.IOException;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Http Response Error Handling class to customize error
 * from Rest API
 */
@EBean
public class HttpResponseErrorHandler implements ResponseErrorHandler {

    /**
     * Injects the instance of current active root context
     */
    @RootContext
    Context context;

    /**
     * Injects the Instance of utility classes
     */
    @Bean
    RestApiUtils restApiUtils;
    @Bean
    AndroidUtils androidUtils;

    /**
     * Overrides the default hasError of ResponseErrorHandler to customize
     * the checking of error
     * Calls RestApiUtils for checking
     * returns true | false
     */
    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return restApiUtils.isError(response.getStatusCode());
    }

    /**
     * Overrides the default handleError of ResponseErrorHandler to customize
     * the error handling
     */
    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        // check first if it is really an error by its status code
        if (restApiUtils.isError(response.getStatusCode())){
            // get the response body as string
            String responseBody = restApiUtils.getResponseBodyAsString(response.getBody());
            // initialize jackson mapper to map string response to the ErrorResponseModel
            ObjectMapper objectMapper = new ObjectMapper();
            ErrorResponseModel errorResponseModel = objectMapper.readValue(responseBody, ErrorResponseModel.class);

            // throw new Exception, this will be handles in the HttpRestErrorHandler
            throw new RestClientException(null, new ApiException(errorResponseModel.getMessage()));
        }
    }

}
