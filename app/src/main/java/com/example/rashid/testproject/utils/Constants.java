package com.example.rashid.testproject.utils;

import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Class for common constants
 */
public final class Constants {

    // other needed permission here
    public static final String[] PERMISSIONS = {
            INTERNET,
            READ_EXTERNAL_STORAGE,
            ACCESS_NETWORK_STATE
    };

    public static final class Common {
        public static final String IMAGE_TYPE = "image/*";
    }

    public static final class RestApi {
        public static final String CONTENT_TYPE = "Content-Type";
        public static final String BEARER = "Bearer ";
    }

    public static final class Drawable {
        public static final String DRAWABLE = "drawable";
    }
}
