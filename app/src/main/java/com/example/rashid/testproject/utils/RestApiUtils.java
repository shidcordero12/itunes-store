package com.example.rashid.testproject.utils;

import org.androidannotations.annotations.EBean;
import org.springframework.http.HttpStatus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Class for Rest API utility methods
 */
@EBean
public class RestApiUtils {

    /**
     * Method for getting the response body as string
     * @param inputStream the stream value
     * @return String the string data
     */
    public String getResponseBodyAsString(InputStream inputStream) {
        // initialize input stream max value
        inputStream.mark(Integer.MAX_VALUE);
        // initialize buffered reader
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
        // initialize string builder
        StringBuilder strBuilder = new StringBuilder();
        String line;
        try {
            // loop to get all the response body string
            while ((line = r.readLine()) != null)
                strBuilder.append(line);
        } catch (IOException ignored) {}
        try {
            inputStream.reset();
        } catch (IOException ignored) {}
        // return the response body as string
        return strBuilder.toString();
    }

    /**
     * Method for checking if Http Request has error
     * @param status the http status value
     * @return boolean true - has error | false - no error
     */
    public boolean isError(HttpStatus status) {
        HttpStatus.Series series = status.series();
        return (HttpStatus.Series.CLIENT_ERROR.equals(series)
                || HttpStatus.Series.SERVER_ERROR.equals(series));
    }
}
