package com.example.rashid.testproject.ui.track.detail;

import android.annotation.SuppressLint;
import android.view.MenuItem;

import com.example.rashid.testproject.R;
import com.example.rashid.testproject.domain.SharedPreferenceService;
import com.example.rashid.testproject.model.response.TrackResponseModel;
import com.example.rashid.testproject.ui.track.list.TrackListActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * An activity representing a single Track Item detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link TrackListActivity}.
 */
@SuppressLint("Registered")
@EActivity(R.layout.activity_track_detail)
public class TrackDetailActivity extends AppCompatActivity {

    /**
     * Inject extras
     */
    @Extra
    TrackResponseModel trackResponseModel;

    /**
     * Inject views
     */
    @ViewById(R.id.tbDetailToolbar)
    Toolbar tbDetailToolbar;

    /**
     * Injects the service classes
     */
    @Bean
    SharedPreferenceService sharedPreferenceService;

    /**
     * Method for initializing view.
     * Run after layout is set
     */
    @AfterViews
    void init(){
        setupToolbar();
        setupDetailTrack();
    }

    /**
     * Setups the application toolbar
     */
    private void setupToolbar(){
        // set toolbar as action bar
        setSupportActionBar(tbDetailToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // enable back button
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Setups the track details on a fragment
     */
    private void setupDetailTrack(){
        if (trackResponseModel != null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            ItemDetailFragment fragment = ItemDetailFragment_.builder()
                    .setOneFragmentArg(trackResponseModel)
                    .build();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.svItemDetailContainer, fragment)
                    .commit();

            // save the last visited page on shared preference
            sharedPreferenceService.setLastVisitedPage(trackResponseModel.getTrackId());
        }
    }

    /**
     * Override back button press to clear last visited page
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sharedPreferenceService.clearLastVisitedPage();
    }

    /**
     * Override onOptionsItemSelected button press to clear last visited page
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            sharedPreferenceService.clearLastVisitedPage();
        }
        return super.onOptionsItemSelected(item);
    }
}
