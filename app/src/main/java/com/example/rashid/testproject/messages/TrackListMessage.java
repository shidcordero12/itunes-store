package com.example.rashid.testproject.messages;

import com.example.rashid.testproject.model.response.TrackResponseModel;

import java.util.List;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Message class for Track List
 */
public class TrackListMessage extends BaseMessage{

    /**
     * Holds the instance of track list to be pass
     */
    private List<TrackResponseModel> trackList;

    /**
     * Method for getting the track list
     * @return List<TrackResponseModel> list of track
     */
    public List<TrackResponseModel> getTrackList() {
        return trackList;
    }

    /**
     * Constructor method for TrackListMessage
     * @param sender the sender of the message
     * @param trackList the list of track to be pass
     */
    public TrackListMessage(Object sender, List<TrackResponseModel> trackList) {
        super(sender);

        this.trackList = trackList;
    }
}
