package com.example.rashid.testproject.base;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 *
 * Base View Wrapper for Recycler View Class
 */
public class BaseViewWrapper<V extends View> extends RecyclerView.ViewHolder {

    /**
     * Holds the instance of a dynamic view
     */
    private V view;

    /**
     * BaseView constructor
     */
    public BaseViewWrapper(V itemView) {
        super(itemView);
        view = itemView;
    }

    /**
     * Returns the current view
     */
    public V getView() {
        return view;
    }
}
