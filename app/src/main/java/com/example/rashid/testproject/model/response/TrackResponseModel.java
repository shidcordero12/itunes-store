package com.example.rashid.testproject.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Model class for storing track response
 */
public class TrackResponseModel implements Parcelable {
    /**
     * Holds the track id
     */
    private float trackId;
    /**
     * Holds the track name
     */
    private String trackName;
    /**
     * Holds the track image url
     */
    private String artworkUrl;
    /**
     * Holds the track price
     */
    private String trackPrice;
    /**
     * Holds the track genre
     */
    private String genre;
    /**
     * Holds the track description
     */
    private String longDescription;

    /**
     * Constructor method for Jackson Mapping
     */
    public TrackResponseModel() {
        super();
    }

    /**
     * Constructor method for TrackResponseModel
     */
    public TrackResponseModel(float trackId, String trackName, String artworkUrl, String trackPrice, String genre, String longDescription) {
        this.trackId = trackId;
        this.trackName = trackName;
        this.artworkUrl = artworkUrl;
        this.trackPrice = trackPrice;
        this.genre = genre;
        this.longDescription = longDescription;
    }

    /**
     * Method for getting the track id
     * @return int the track id value
     */
    public float getTrackId() {
        return trackId;
    }

    /**
     * Method for setting the track id value
     * Annotated as "trackId" for Jackson Mapping
     * @param trackId the track id value
     */
    @JsonProperty("trackId")
    public void setTrackId(float trackId) {
        this.trackId = trackId;
    }

    /**
     * Method for getting the track name
     * @return int the track name value
     */
    public String getTrackName() {
        return trackName;
    }

    /**
     * Method for setting the track name value
     * Annotated as "trackName" for Jackson Mapping
     * @param trackName the track name value
     */
    @JsonProperty("trackName")
    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    /**
     * Method for getting the track artwork url
     * @return int the track artwork url value
     */
    public String getArtworkUrl() {
        return artworkUrl;
    }

    /**
     * Method for setting the track artwork url value
     * Annotated as "artworkUrl100" for Jackson Mapping
     * @param artworkUrl the track artwork url value
     */
    @JsonProperty("artworkUrl100")
    public void setArtworkUrl(String artworkUrl) {
        this.artworkUrl = artworkUrl;
    }

    /**
     * Method for getting the track price
     * @return int the track price value
     */
    public String getTrackPrice() {
        return trackPrice;
    }

    /**
     * Method for setting the track price value
     * Annotated as "trackPrice" for Jackson Mapping
     * @param trackPrice the track price value
     */
    @JsonProperty("trackPrice")
    public void setTrackPrice(String trackPrice) {
        this.trackPrice = trackPrice;
    }

    /**
     * Method for getting the track genre
     * @return int the track genre value
     */
    public String getGenre() {
        return genre;
    }

    /**
     * Method for setting the track genre value
     * Annotated as "primaryGenreName" for Jackson Mapping
     * @param genre the track genre value
     */
    @JsonProperty("primaryGenreName")
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * Method for getting the track description
     * @return int the track description value
     */
    public String getLongDescription() {
        return longDescription;
    }

    /**
     * Method for setting the track description value
     * Annotated as "longDescription" for Jackson Mapping
     * @param longDescription the track description value
     */
    @JsonProperty("longDescription")
    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(this.trackId);
        dest.writeString(this.trackName);
        dest.writeString(this.artworkUrl);
        dest.writeString(this.trackPrice);
        dest.writeString(this.genre);
        dest.writeString(this.longDescription);
    }

    /**
     * Constructor for Parcel bundling
     * @param in parcel instance
     */
    private TrackResponseModel(Parcel in) {
        this.trackId = in.readFloat();
        this.trackName = in.readString();
        this.artworkUrl = in.readString();
        this.trackPrice = in.readString();
        this.genre = in.readString();
        this.longDescription = in.readString();
    }

    /**
     * Parcelable creator for bundling
     */
    public static final Parcelable.Creator<TrackResponseModel> CREATOR = new Parcelable.Creator<TrackResponseModel>() {
        @Override
        public TrackResponseModel createFromParcel(Parcel source) {
            return new TrackResponseModel(source);
        }

        @Override
        public TrackResponseModel[] newArray(int size) {
            return new TrackResponseModel[size];
        }
    };
}
