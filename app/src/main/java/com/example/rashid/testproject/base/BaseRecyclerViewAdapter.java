package com.example.rashid.testproject.base;

import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Base Recycler View Adapter class
 */
public abstract class BaseRecyclerViewAdapter<T, V extends View> extends RecyclerView.Adapter<BaseViewWrapper<V>> {

    /**
     * Holds the instance of a dynamic list
     */
    protected List<T> items = new ArrayList<>();


    /**
     * Returns the number of items stored in the list
     */
    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * Overrides onCreateViewHolder to use the overriden method from child class
     */
    @Override
    public final BaseViewWrapper<V> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BaseViewWrapper<>(onCreateItemView(parent, viewType));
    }

    /**
     * Abstract method for onCreateItemView for child class to override
     */
    protected abstract V onCreateItemView(ViewGroup parent, int viewType);
}
