package com.example.rashid.testproject.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.example.rashid.testproject.R;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatTextView;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Class for dialog utility methods
 */
@EBean
public class DialogUtils {

    /**
     * Holds the instance of application root context
     */
    @RootContext
    Context context;

    /**
     * Holds the instance of application current activity
     */
    @RootContext
    Activity activity;

    /**
     * Declare dialog private property
     */
    private AlertDialog dialog;

    /**
     * Method for showing a Loading dialog
     * @param message the message to be shown while loading
     */
    @SuppressLint("InflateParams")
    public void showLoadingDialog(int message){
        // initialize layout inflater
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_loading, null);

        // set the message
        AppCompatTextView textView = dialogView.findViewById(R.id.tvMessage);
        textView.setText(message);

        // initialize dialog builder and show the dialog
        dialog = new AlertDialog.Builder(context, R.style.Theme_AppCompat_Light_Dialog)
            .setView(dialogView)
            .setCancelable(false)
            .show();
    }

    /**
     * Method for showing an Info Dialog
     * @param title the title to be shown
     * @param message the message to be shown
     */
    public void showInfoDialog(String title, String message){
        // initialize dialog builder and show the dialog
        dialog = new AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(R.string.label_ok, null)
            .setIcon(R.drawable.blue_info_icon)
            .show();
    }

    /**
     * Method for showing an Warning Dialog
     * @param title the title to be shown
     * @param message the message to be shown
     */
    public void showWarningDialog(String title, String message){
        // initialize dialog builder and show the dialog
        dialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.label_ok, null)
                .setIcon(R.drawable.red_warning_icon)
                .show();
    }

    /**
     * Method for showing an Confirmation Dialog
     * @param title the title to be shown
     * @param message the message to be shown
     */
    public void showConfirmationDialog(String title, String message, int positiveText){
        // initialize dialog builder and show the dialog
        dialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveText, null)
                .setNegativeButton(R.string.label_cancel, null)
                .setIcon(R.drawable.gray_confirmation_icon)
                .show();
    }

    /**
     * Common method for dismissing the dialog
     */
    public void dismissDialog(){
        if (dialog.isShowing()){
            dialog.dismiss();
        }
    }

    /**
     * Method for showing and dismissing loading dialog
     * Annotated as @UiThread to be able to use inside @Background Annotated method
     * @param isShow the boolean value if to show a dialog or not
     */
    @UiThread
    public void publishProgress(boolean isShow) {
        if (isShow){
            showLoadingDialog(R.string.cm_loading_label);
        } else {
            dismissDialog();
        }
    }
}
