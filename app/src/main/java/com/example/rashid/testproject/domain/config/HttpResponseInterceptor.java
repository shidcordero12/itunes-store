package com.example.rashid.testproject.domain.config;

import android.accounts.NetworkErrorException;

import com.example.rashid.testproject.R;
import com.example.rashid.testproject.utils.AndroidUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestClientException;

import java.io.IOException;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Http Response Interceptor to allow intercept on response
 * or request for Http API calls
 */
@EBean
public class HttpResponseInterceptor implements ClientHttpRequestInterceptor {

    /**
     * Injects the utility classes
     */
    @Bean
    AndroidUtils androidUtils;

    /**
     * Overrides the default Http Interceptor
     */
    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        // check if there's internet connection available
        if (androidUtils.isInternetConnected()) {
            // execute the Http Request
            return execution.execute(request, body);
        }
        // throw exception for no internet connectivity, this will be handled by the HttpRestErrorHandler
        throw new RestClientException(null, new NetworkErrorException(androidUtils.getMessage(R.string.em_no_internet)));
    }
}
