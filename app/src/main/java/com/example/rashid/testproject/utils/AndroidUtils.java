package com.example.rashid.testproject.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.example.rashid.testproject.R;
import com.google.android.material.snackbar.Snackbar;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Class for common android utility methods
 */
@EBean
public class AndroidUtils {
    @RootContext
    Context context;

    private static Snackbar snackbar;

    /**
     * Method for checking length of text
     * @param text the text to be checked
     * @param minLength the minimum length
     * @param maxLength the maximum length
     * @return boolean true - has error, false - no error
     */
    public boolean isLengthError(String text, int minLength, int maxLength){
        return text.length() < minLength || text.length() > maxLength;
    }

    /**
     * Method for showing internet status in Snackbar
     * @param view the root view
     * @param isConnected the status
     */
    public void showInternetError(View view, boolean isConnected){
        // if connected to internet
        if (isConnected) {
            // call method to hide snackbar
            hideSnackBar();
        } else {
            // call method to show snackbar
            showSnackBar(view, getMessage(R.string.em_no_internet));
        }
    }

    /**
     * Method to show snackbar
     * @param view the root view
     * @param snackTitle the snackbar title
     */
    public void showSnackBar(View view, String snackTitle){
        // initialize snackbar
        snackbar = Snackbar.make(view, snackTitle, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.cm_dismiss_button, v -> hideSnackBar())
                .setActionTextColor(view.getResources().getColor(android.R.color.holo_blue_light));

        // set snackbar text
        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        // show snackbar
        snackbar.show();
    }

    /**
     * Method to hide the snackbar
     */
    public void hideSnackBar(){
        // Hides snackbar if it is initialize or is shown
        if (snackbar != null && snackbar.isShown()){
            snackbar.dismiss();
        }
    }

    /**
     * Method for getting image drawable id by its drawable name
     * @param imageName the drawable name
     * @return int the drawable it
     */
    public int getImageDrawable(String imageName){
        return context.getResources().getIdentifier(imageName, Constants.Drawable.DRAWABLE, context.getPackageName());
    }

    /**
     * Method for getting the message value by its string id
     * @param stringId the string id value
     * @return String the string value
     */
    public String getMessage(int stringId){
        return context.getResources().getString(stringId);
    }

    /**
     * Method for checking if user is connected to the internet
     * @return boolean true - connected | false - not connected
     */
    public boolean isInternetConnected(){
        // initialize connectivity manager
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        // check network info
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        // return network info if connected or is connecting
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
