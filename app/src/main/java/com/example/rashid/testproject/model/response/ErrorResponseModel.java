package com.example.rashid.testproject.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Model class for storing error response
 */
public class ErrorResponseModel {

    /**
     * Holds the error code
     */
    private String code;
    /**
     * Holds the error message
     */
    private String message;
    /**
     * Holds other data
     */
    private String data;

    /**
     * Constructor method for Jackson Mapping
     */
    public ErrorResponseModel() {
        super();
    }

    /**
     * Constructor method for ErrorResponseModel
     */
    public ErrorResponseModel(String code, String message, String data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * Method for getting the error code
     * @return String the error code value
     */
    public String getCode() {
        return code;
    }

    /**
     * Method for setting the error code value
     * Annotated as "code" for Jackson Mapping
     * @param code the error code value
     */
    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Method for getting the error message
     * @return String error message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Method for setting the error message value
     * Annotated as "message" for Jackson Mapping
     * @param message the error message value
     */
    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Method for getting the data value
     * @return String the data value
     */
    public String getData() {
        return data;
    }

    /**
     * Method for setting the data value
     * Annotated as "data" for Jackson Mapping
     * @param data String the data value
     */
    @JsonProperty("data")
    public void setData(String data) {
        this.data = data;
    }
}
