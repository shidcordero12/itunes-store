package com.example.rashid.testproject.domain.local;

import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Last Visited Shared Preference interface.
 * This is the unsecured way since data is stored internally
 * without encryption.
 */
@SharedPref
public interface LastVisitedSharedPref {

    /**
     * The field name will have default value "" (empty string)
     * This should be float but because the id return from the api
     * has the same value for a float default value(0), therefore
     * using String as data type would fix the issue.
     */
    @DefaultString("")
    String trackId();
}