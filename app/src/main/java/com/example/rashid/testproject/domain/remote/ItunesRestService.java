package com.example.rashid.testproject.domain.remote;

import com.example.rashid.testproject.BuildConfig;
import com.example.rashid.testproject.domain.config.HttpResponseErrorHandler;
import com.example.rashid.testproject.domain.config.HttpResponseInterceptor;

import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.androidannotations.rest.spring.api.RestClientHeaders;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Itunes Rest Service interface for Http API Call
 */
@Rest(rootUrl = BuildConfig.API_URL, // Stored in build.gradle
        converters = {
            MappingJackson2HttpMessageConverter.class, // String to JSON mapper for converting API response
            StringHttpMessageConverter.class}, // String converter for String type API response
        interceptors = { HttpResponseInterceptor.class }, // injecting Custom interceptor
        responseErrorHandler = HttpResponseErrorHandler.class) // injecting custom response error handler
public interface ItunesRestService  extends RestClientHeaders, RestClientErrorHandling {

    /**
     * Search a Track from Itunes Store
     * @return String value of Content-Type text/javascript
     */
    @Get("/search?term=star&amp;country=au&amp;media=movie")
    String search();
}
