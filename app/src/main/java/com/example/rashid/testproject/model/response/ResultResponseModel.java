package com.example.rashid.testproject.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Model class for storing result response
 */
public class ResultResponseModel {
    /**
     * Holds the result count
     */
    private int resultCount;
    /**
     * Holds the list of tracks data
     */
    private List<TrackResponseModel> trackList;

    /**
     * Constructor method for Jackson Mapping
     */
    public ResultResponseModel() {
        super();
    }

    /**
     * Constructor method for ResultResponseModel
     */
    public ResultResponseModel(int resultCount, List<TrackResponseModel> trackList) {
        this.resultCount = resultCount;
        this.trackList = trackList;
    }

    /**
     * Method for getting the result count
     * @return int the result count value
     */
    public int getResultCount() {
        return resultCount;
    }

    /**
     * Method for setting the result count value
     * Annotated as "resultCount" for Jackson Mapping
     * @param resultCount the result count value
     */
    @JsonProperty("resultCount")
    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    /**
     * Method for getting the track list value
     * @return  List<TrackResponseModel> the track list value
     */
    public List<TrackResponseModel> getTrackList() {
        return trackList;
    }

    /**
     * Method for setting the track list value
     * Annotated as "results" for Jackson Mapping
     * @param trackList the track list value
     */
    @JsonProperty("results")
    public void setTrackList(List<TrackResponseModel> trackList) {
        this.trackList = trackList;
    }
}
