package com.example.rashid.testproject.ui.track.detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.rashid.testproject.R;
import com.example.rashid.testproject.model.response.TrackResponseModel;
import com.example.rashid.testproject.ui.track.list.TrackListActivity;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.appbar.SubtitleCollapsingToolbarLayout;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link TrackListActivity}
 * in two-pane mode (on tablets) or a {@link TrackDetailActivity}
 * on handsets.
 */
@EFragment
public class ItemDetailFragment extends Fragment {

    /**
     * Declare private properties
     */
    private SubtitleCollapsingToolbarLayout appBarLayout;
    private AppCompatImageView ivArtWork;
    private TrackResponseModel trackResponseModel;

    /**
     * Inject fragment arguments
     * @param trackResponseModel TrackResponseModel from TrackListActivity or TrackDetailActivity
     */
    @FragmentArg("trackResponseModel")
    void setOneFragmentArg(TrackResponseModel trackResponseModel){
        this.trackResponseModel = trackResponseModel;

        // initialize fragment layout
        init();
    }

    /**
     * Method for initializing fragment layout
     */
    private void init(){
        // initialize layout fields from root activity
        appBarLayout = Objects.requireNonNull(getActivity()).findViewById(R.id.tlToolbar);
        ivArtWork = getActivity().findViewById(R.id.ivArtWork);
        // set track details data
        if (appBarLayout != null) {
            // set data to the layout fields
            appBarLayout.setTitle(trackResponseModel.getTrackName());
            appBarLayout.setSubtitle(trackResponseModel.getGenre() + " " + getResources().getString(R.string.tl_buy2) + trackResponseModel.getTrackPrice());

            // load image using Glide for caching image
            Glide.with(Objects.requireNonNull(getContext())).load(trackResponseModel.getArtworkUrl()).apply(
                    new RequestOptions()
                            .fitCenter() // placeholder and error image will be fir on center
                            .placeholder(R.drawable.icon_image_placeholder) // sets the image placeholder
                            .error(R.drawable.icon_image_broken) // set the image when there's an error
            ).into(ivArtWork);

        }
    }


    /**
     * Overrides method to inflate custom view layout
     * @param inflater instance of layout inflater
     * @param container instance of view group container
     * @param savedInstanceState instance of bundle
     * @return View instance
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);

        if (trackResponseModel != null) {
            ((TextView) rootView.findViewById(R.id.tvLongDescription)).setText(trackResponseModel.getLongDescription());
        }

        return rootView;
    }
}
