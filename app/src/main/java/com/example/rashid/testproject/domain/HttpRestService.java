package com.example.rashid.testproject.domain;

import com.example.rashid.testproject.domain.config.HttpRestErrorHandler;
import com.example.rashid.testproject.domain.remote.ItunesRestService;
import com.example.rashid.testproject.messages.ErrorMessage;
import com.example.rashid.testproject.messages.TrackListMessage;
import com.example.rashid.testproject.model.response.ResultResponseModel;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmtron.greenannotations.EventBusGreenRobot;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.rest.spring.annotations.RestService;
import org.greenrobot.eventbus.EventBus;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Https Rest Service for all http call
 */
@EBean(scope = EBean.Scope.Singleton)
public class HttpRestService {

    /**
     * Injects event bus for pub/sub functionality
     */
    @EventBusGreenRobot
    EventBus eventBus;

    /**
     * Injects Http Rest error handler
     */
    @Bean
    HttpRestErrorHandler restErrorHandler;

    /**
     * Inject the Itunes Rest Service
     */
    @RestService
    ItunesRestService itunesRestService;

    /**
     * This method runs after successful injection
     * of annotated classes.
     * This method set the RestErrorHandler for Rest Service
     */
    @AfterInject
    void setRestErrorHandler() {
        itunesRestService.setRestErrorHandler(restErrorHandler);
    }

    /**
     * Public method that will get the search result for
     * Itunes track list
     */
    public void search() {
        try{
            // Calls the API and read the return as string (api return Content-Type as type/javascript
            String response = itunesRestService.search();
            // Initialize Jackson mapper to map return String to a model class
            ObjectMapper objectMapper = new ObjectMapper();
            // disable Deserialization of unknown property (properties that does not exists in the model class)
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            // map the String response to the model class
            ResultResponseModel resultResponseModel = objectMapper.readValue(response, ResultResponseModel.class);
            // publish a message passing the list of track
            eventBus.post(new TrackListMessage(this, resultResponseModel.getTrackList()));
        } catch(Exception ex){
            // publish a message passing the error message
            eventBus.post(new ErrorMessage(this, ex.getMessage()));
        }
    }

}
