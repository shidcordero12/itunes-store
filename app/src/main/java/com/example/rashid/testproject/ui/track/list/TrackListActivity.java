package com.example.rashid.testproject.ui.track.list;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.FrameLayout;

import com.example.rashid.testproject.R;
import com.example.rashid.testproject.domain.HttpRestService;
import com.example.rashid.testproject.domain.SharedPreferenceService;
import com.example.rashid.testproject.exception.SharedPreferenceException;
import com.example.rashid.testproject.messages.ErrorMessage;
import com.example.rashid.testproject.messages.TrackListMessage;
import com.example.rashid.testproject.model.response.TrackResponseModel;
import com.example.rashid.testproject.ui.track.detail.TrackDetailActivity;
import com.example.rashid.testproject.utils.AndroidUtils;
import com.example.rashid.testproject.utils.Constants;
import com.example.rashid.testproject.utils.DialogUtils;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.tmtron.greenannotations.EventBusGreenRobot;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static br.com.zbra.androidlinq.Linq.stream;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * An activity representing a list of Track Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link TrackDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
@SuppressLint("Registered")
@EActivity(R.layout.activity_track_list)
public class TrackListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean isTwoPane;
    /**
     * Sets the number of items to be shown in a row.
     * Default value is 2 for a Single Pane view
     */
    private int gridSize = 2;
    /**
     * Holds the list adapter instance
     */
    private TrackListAdapter trackListAdapter;

    /**
     * Initialize view
     */
    @ViewById(R.id.tbToolbar)
    Toolbar tbToolbar;
    @ViewById(R.id.flTrackDetailContainer)
    FrameLayout flTrackDetailContainer;
    @ViewById(R.id.rvTrackList)
    RecyclerView rvTrackList;

    /**
     * Injects the utility classes
     */
    @Bean
    AndroidUtils androidUtils;
    @Bean
    DialogUtils dialogUtils;

    /**
     * Injects the service classes
     */
    @Bean
    HttpRestService httpRestService;
    @Bean
    SharedPreferenceService sharedPreferenceService;

    /**
     * Injects event bus for pub/sub functionality
     */
    @EventBusGreenRobot
    EventBus eventBus;


    /**
     * Method called after layout is set
     */
    @AfterViews
    void init(){
        setupToolbar();
        setupMasterDetail();
        setupTrackList();
        askRequiredPermission();
    }

    /**
     * Setups the application toolbar
     */
    private void setupToolbar(){
        // set tbToolbar as the default toolbar
        setSupportActionBar(tbToolbar);
        // set the toolbar title
        tbToolbar.setTitle(getTitle());
    }

    /**
     * Setups the MasterDetail view
     */
    private void setupMasterDetail(){
        if (flTrackDetailContainer != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            isTwoPane = true;
            // set gridSize to 4, 4 items will be shown in a row
            gridSize = 4;
        }
    }

    /**
     * Setups the Track List View
     */
    private void setupTrackList() {
        // rvTrackList should not be null
        assert rvTrackList != null;
        // initialize Recycler View Adapter
        trackListAdapter = new TrackListAdapter(this, isTwoPane);

        // initialize GridLayoutManager
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, gridSize);
        gridLayoutManager.setSpanCount(gridSize);
        // initialize custom divider item decorator
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(this, R.drawable.custom_divider)));
        rvTrackList.addItemDecoration(itemDecorator);

        // set the adapter to the recycler view
        rvTrackList.setAdapter(trackListAdapter);
        // set the layout manager to the recycler view
        rvTrackList.setLayoutManager(gridLayoutManager);
    }

    /**
     * Method to ask required application permission to the user.
     * This is only applicable for Android 5.0 and above
     */
    private void askRequiredPermission(){
        // initialize permission rationale and option messages
        String rationale = androidUtils.getMessage(R.string.im_permission_desc);
        Permissions.Options options = new Permissions.Options()
                .setRationaleDialogTitle(androidUtils.getMessage(R.string.message_title_info))
                .setSettingsDialogTitle(androidUtils.getMessage(R.string.message_title_warning));

        // checks and ask for permission
        Permissions.check(this, Constants.PERMISSIONS, rationale, options, new PermissionHandler() {
            @Override
            public void onGranted() {
                // if all permission is granted, call getTracks() method to get the track list
                getTracks();
            }

            @Override
            public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                // shows a dialog if 1 or more permission is denied
                dialogUtils.showWarningDialog(
                        androidUtils.getMessage(R.string.message_title_warning),
                        androidUtils.getMessage(R.string.im_denied_some_permission));
            }
        });
    }

    /**
     * Method for getting the track list
     * This method is executed on the background
     */
    @Background
    public void getTracks() {
        // show a Loading dialog
        dialogUtils.publishProgress(true);
        // get the track list
        httpRestService.search();
    }

    /**
     * Subscriber method for a successful HttpRestService search() method
     * @param message TrackListMessage the message contains the sender and the track list data
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void populateData(TrackListMessage message){
        // dismiss the Loading dialog
        dialogUtils.dismissDialog();
        // checks if there's a last visited page
        if (sharedPreferenceService.hasLastVisitedPage()){
            float trackId;
            try {
                // gets the track id from the shared preference
                trackId = sharedPreferenceService.getLastVisitedPage();
                // check if the track list has the following track id, this method will return null if no track is found
                TrackResponseModel trackResponseModel =  stream(message.getTrackList()).where(c -> c.getTrackId() == trackId).firstOrNull();
                // if not null redirect to its detailed view
                if (trackResponseModel != null){
                    trackListAdapter.redirectToDetailView(trackResponseModel);
                }
            } catch (SharedPreferenceException e) {
                // this should not be happen, but just encase show a common error message
                dialogUtils.showWarningDialog(androidUtils.getMessage(R.string.message_title_warning), androidUtils.getMessage(R.string.em_error_occurred));
            }
        }
        // add the track list to recycler view
        trackListAdapter.updateTrack(message.getTrackList());
    }

    /**
     * Subscriber method for a failed HttpRestService search() method
     * @param message ErrorMessage the message contains the sender and the error message
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showError(ErrorMessage message){
        // dismiss the Loading Dialog
        dialogUtils.dismissDialog();
        // show a dialog error message
        dialogUtils.showWarningDialog(androidUtils.getMessage(R.string.message_title_error), message.getMessage());
    }
}
