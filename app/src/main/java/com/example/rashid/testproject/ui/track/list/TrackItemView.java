package com.example.rashid.testproject.ui.track.list;

import android.annotation.SuppressLint;
import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.rashid.testproject.R;
import com.example.rashid.testproject.model.response.TrackResponseModel;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;

/**
 * @author      Rashid Cordero <shidcordero12@gmail.com>
 * @version     1.0
 * @since       1.0
 * Item View class for binding details to layout fields
 */
@EViewGroup(R.layout.row_track_content)
public class TrackItemView extends ConstraintLayout {

    /**
     * Injects all fields
     */
    @ViewById(R.id.ivArtWork)
    AppCompatImageView ivArtWork;
    @ViewById(R.id.tvTrackName)
    AppCompatTextView tvTrackName;
    @ViewById(R.id.tvTrackPrice)
    AppCompatTextView tvTrackPrice;
    @ViewById(R.id.tvGenre)
    AppCompatTextView tvGenre;

    /**
     * Constructor method for TrackItemView
     * @param context instance of context
     */
    public TrackItemView(Context context) {
        super(context);
    }

    /**
     * Method for binding the model data to the layout fields
     * @param trackResponseModel
     */
    @SuppressLint("SetTextI18n")
    public void bind(TrackResponseModel trackResponseModel){
        // set data to the layout fields
        tvTrackName.setText(trackResponseModel.getTrackName());
        tvGenre.setText(trackResponseModel.getGenre());
        tvTrackPrice.setText(getResources().getString(R.string.tl_buy) + trackResponseModel.getTrackPrice());

        // load image using Glide for caching image
        Glide.with(this).load(trackResponseModel.getArtworkUrl()).apply(
                new RequestOptions()
                        .fitCenter() // placeholder and error image will be fir on center
                        .placeholder(R.drawable.icon_image_placeholder) // sets the image placeholder
                        .error(R.drawable.icon_image_broken) // set the image when there's an error
        ).into(ivArtWork);
    }
}
